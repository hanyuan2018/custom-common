package com.aries.custom.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类
 *
 * @author hanyuan
 * @date Created in 2020-12-20 22:32:05
 * @since 1.0.0-SNAPSHOT
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
